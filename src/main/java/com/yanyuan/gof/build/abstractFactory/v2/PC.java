package com.yanyuan.gof.build.abstractFactory.v2;

/**
 * @Description Abstract ProductA
 * @Author yanyuan
 * @Date 23:00 2020/4/17
 * @Version 1.0
 **/
public interface PC {
    void playGame();
}
