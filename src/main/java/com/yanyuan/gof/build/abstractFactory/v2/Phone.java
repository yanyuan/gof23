package com.yanyuan.gof.build.abstractFactory.v2;

/**
 * @Description ProductB 抽象产品类
 * @Author yanyuan
 * @Date 18:27 2020/4/14
 * @Version 1.0
 **/
public interface Phone {
    void call(String receiver);
}
