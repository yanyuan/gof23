package com.yanyuan.gof.build.builder;

import lombok.Data;

/**
 * @Description 产品角色
 * @Author yanyuan
 * @Date 09:40 2020/4/7
 * @Version 1.0
 **/
@Data
public class Robot {
    private String head;
    private String body;
    private String legs;
}
