package com.yanyuan.gof.structural.adapter;

/**
 * @Description Target
 * @Author yanyuan
 * @Date 22:49 2020/4/7
 * @Version 1.0
 **/
public interface Car4s {
    Car changeColor(Car car, String color);
}
